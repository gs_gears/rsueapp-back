from datetime import timedelta

from flask import Flask, jsonify
from flask_restful import Api
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy

from models import db_session
from models.token import RevokedTokenModel
from resources import login_resource, book_resource

app = Flask(__name__)
app.config['SECRET_KEY'] = 'glushenko_secret_key'
app.config['JWT_SECRET_KEY'] = 'jwt-secret-glushenko'
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=1)
app.config["JWT_REFRESH_TOKEN_EXPIRES"] = timedelta(days=1)
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

conn_str = db_session.global_init()
app.config['SQLALCHEMY_DATABASE_URI'] = conn_str
db = SQLAlchemy(app)

migrate = Migrate(app, db_session)
api = Api(app, catch_all_404s=True)
jwt = JWTManager(app)

api.add_resource(login_resource.UserRegistration, '/registration')
api.add_resource(login_resource.UserLogin, '/login')
api.add_resource(login_resource.UserLogoutAccess, '/logout/access')
api.add_resource(login_resource.UserLogoutRefresh, '/logout/refresh')
api.add_resource(login_resource.TokenRefresh, '/token/refresh')
api.add_resource(login_resource.AllUsers, '/users')
api.add_resource(login_resource.SecretResource, '/secret')
# -------------------------------------------------------------------------
api.add_resource(book_resource.BookListResource, '/api/books')
api.add_resource(book_resource.BookResource, '/api/books/<book_id>')


@jwt.token_in_blocklist_loader
def check_if_token_is_revoked(jwt_header, jwt_payload):
    jti = jwt_payload["jti"]
    token = RevokedTokenModel.is_jti_blacklisted(jti)
    return token is not None


@app.route('/')
def hello_world():
    return jsonify(message='Hello, World!')


def main():
    app.run()


if __name__ == '__main__':
    main()
