import uuid

from flask import jsonify
from flask_restful import abort, Resource

from models import db_session
from models.books import Book
from resources.book_reqparse import parser


def abort_if_news_not_found(book, id):
    if not book:
        abort(404, message=f"Book {id} not found")


def find_book(book_id):
    session = db_session.create_session()
    book = session.query(Book).get(book_id)
    return book


class BookResource(Resource):
    def get(self, book_id):
        book_uuid = uuid.UUID(book_id)
        book = find_book(book_uuid)
        abort_if_news_not_found(book, book_id)
        return jsonify(book.to_dict(
            only=('id', 'title', 'date', 'isReaded')))  # 'userId',

    def put(self, book_id):
        args = parser.parse_args()
        book_uuid = uuid.UUID(book_id)
        book = find_book(book_uuid)
        abort_if_news_not_found(book, book_id)
        book.title = args['title']
        book.date = args['date']
        book.isReaded = True if args['isReaded'] == 'True' else False
        book.update_to_db()
        return jsonify({'success': 'OK'})

    def delete(self, book_id):
        book_uuid = uuid.UUID(book_id)
        book = find_book(book_uuid)
        abort_if_news_not_found(book, book_id)
        book.delete_from_db()
        return jsonify({'success': 'OK'})


class BookListResource(Resource):
    def get(self):
        session = db_session.create_session()
        books = session.query(Book).all()
        return jsonify([item.to_dict(
            # ) for item in books])
            only=('id', 'title', 'date', 'isReaded')) for item in books])

    #         'user.name', 'user.email',

    def post(self):
        args = parser.parse_args()
        book = Book(**args)
        # book = Book(
        #     title=args['title'],
        #     date=args['date'],
        #     userId=args['userId']
        # )
        book.save_to_db()
        return jsonify({'success': 'OK'})
