import datetime
import uuid

import sqlalchemy
from sqlalchemy import orm
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy_serializer import SerializerMixin

from models import db_session
from .db_session import SqlAlchemyBase


class Book(SqlAlchemyBase, SerializerMixin):
    __tablename__ = 'books'

    id = sqlalchemy.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    title = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    date = sqlalchemy.Column(sqlalchemy.DateTime, default=datetime.datetime.now)
    isReaded = sqlalchemy.Column(sqlalchemy.Boolean, default=False)

    # is_published = sqlalchemy.Column(sqlalchemy.Boolean, default=True)
    userId = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey("users.id"))
    user = orm.relation('User')

    def save_to_db(self):
        session = db_session.create_session()
        session.add(self)
        session.commit()

    def update_to_db(self):
        session = db_session.create_session()
        session.merge(self)
        session.commit()

    def delete_from_db(self):
        session = db_session.create_session()
        session.delete(self)
        session.commit()
